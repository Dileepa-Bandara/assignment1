#include <stdio.h>
#include <stdlib.h>

int main()
{
    int number,remainder,output;

    printf("Pls enter your number :");
    scanf("%d",&number);

    output = 0;

    while(number >= 1)
    {
        //get remainder from the number series
        remainder = number %10;
        //multiply previous remainder by 10 and add new remainder to it
        output = output*10 + remainder ;
        //separate remainder to get new remainder
        number = number / 10;

    }
        //print the answer
        printf("Reversed number is %d",output);
    return 0;
}
