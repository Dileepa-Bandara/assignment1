#include <stdio.h>
#include <stdlib.h>

int main()
{    //create varibales
    int FirstNumber,SecondNumber,swap;
     //get inputs from user
    printf("Enter the first integer       :");
    scanf(" %d",&FirstNumber);

    printf("Enter the second integer      :");
    scanf(" %d",&SecondNumber);

    //before  swapping integers
    printf("===================before swapping============================");
    printf("\nBefore swapping first integer  = %d\n",FirstNumber);
    printf("Before swapping second integer = %d\n",SecondNumber);

    //using temporary variables
    swap=FirstNumber;
    FirstNumber=SecondNumber;
    SecondNumber=swap;

    //after swapping

    printf("===================after swapping=============================");
    printf("\nAfter swapping first integer   = %d\n",FirstNumber);
    printf("After swapping second integer  = %d\n",SecondNumber);


    return 0;
}
