#include <stdio.h>
#include <stdlib.h>

int main()
{   //variables
    float r,x,area;


    //get radius of the disc from user
    printf("Enter radius of the disc in cm :");
    scanf(" %f", &r);

    //create formula for the find disc area
    x=r*r;
    area = 3.14 * x;

    //disk area
    printf("The area of the disc is  cm^2  : %.2f",area);


    return 0;
}
