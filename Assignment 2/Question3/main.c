#include <stdio.h>
#include <stdlib.h>

int main()
{   //variables
    char c ;
    int vowel;

    printf("Enter your English alphabet character  :");
    scanf("%c",&c);

    vowel = (c == 'a' || c == 'e' ||  c == 'i' || c == 'o' || c == 'u' || c == 'A'|| c == 'E'|| c == 'I' || c == 'O' || c == 'U');
      //conditions
    if (vowel){
        printf("%c is a vowel",c);
    }
    else {
        printf("%c is a consonant",c);
    }
    return 0;
}
